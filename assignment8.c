#include <stdio.h>

struct Student{
    char name[20];
    char subject[10];
    int marks;
};

int main()
{
    int n=6;

    struct Student student[n];

    for(int i=0; i<n; i++)
    {
        printf("\n");
        printf("Enter the name of the Student : ");
        scanf("%s", &student[i].name);

        printf("Enter subject name : ");
        scanf("%s", &student[i].subject);

        printf("Enter the marks : ");
        scanf("%d", &student[i].marks);
    }

    printf("\n");

    for(int i=0; i<n; i++)
    {
        printf("%s's marks : \n%s - %d\n\n", student[i].name, student[i].subject, student[i].marks);
    }

    return 0;
}
